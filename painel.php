<?php 
session_start(); 
//print_r($_SESSION['usuario']);
//include('verifica_login.php');

#connection 
require_once 'conections_db/db_connect.php'; 
//include('verifica_login.php');
#header
include_once 'includes/header.php'; 
include_once 'includes/message.php';
?>

<div class="row">
    <!--<div class="col s12 m6 push-m3"> -->
    <div class="col s12">
        <h3 class='light-grey'><i class="material-icons icon">folder_open</i>Clientes</h3>
        <div class="divider"></div><br>
        <div class="row">
            <div class="col s12">
                <div id="mensage"></div>
            </div>
        </div>
        <div class="col s12">
            <div class="col s12">
                <table id="list" class="responsive-table striped">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Email</th>
                            <th>Telefone</th>
                            <!--<th>Data de Nascimento</th>-->
                            <th>Endereço</th>
                            <th>Cidade</th>
                            <th>Estado</th>
                            <th>Cep</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                        
                    $sql = "SELECT * FROM CLIENTES"; 
                    $resultado = mysqli_query($connect, $sql);

                    if ( mysqli_num_rows( $resultado )  > 0 ):
                    while($dados = mysqli_fetch_array( $resultado )): 
                ?>
                        <tr>
                            <td><?php echo $dados['NOME'];?></td>
                            <td><?php echo $dados['EMAIL'];?></td>
                            <!-- <td><?php echo date("d/m/Y",strtotime($dados['DATA_NASCIMENTO']));?></td> -->
                            <td><?php echo $dados['TELEFONE'];?></td>
                            <td><?php echo $dados['ENDERECO'];?></td>
                            <td><?php echo $dados['CIDADE'];?></td>
                            <td><?php echo $dados['ESTADO'];?></td>
                            <td><?php echo $dados['CEP'];?></td>
                            
                            <td>
                                <a href="edit.php?id=<?php echo $dados['ID'];?>"
                                    class="btn-floating teal darken-2 tooltipped" data-position="top"
                                    data-tooltip="Editar cliente"><i class="material-icons">edit</i></a>
                            </td>
                            <td>
                                <a href="#modal <?php echo $dados['ID'];?>"
                                    class="btn-floating red darken-4 modal-trigger tooltipped" data-position="top"
                                    data-tooltip="Excluir cliente"><i class="material-icons">delete</i></a>
                            </td>
                            <!-- Modal Structure -->
                            <div id="modal <?php echo $dados['ID'];?>" class="modal">
                                <div class="modal-content">
                                    <h4>Oops!</h4>
                                    <p id="messenger">Tem certeza que deseja excluir esse cliente?</p>
                                </div>
                                <div class="modal-footer">
                                    <form action="php_action/delete.php" method="POST">
                                        <input type="hidden" name="id" value="<?php echo $dados['ID'];?>">
                                        <button type="submit" name="btn-deletar" class="btn red">Sim, quero deletar
                                        </button>
                                    </form>
                                    <a href="#!" class="modal-close waves-effect waves-green btn-flat">Cancelar</a>
                                </div>
                            </div>
                        </tr>
                        <?php 
                    endwhile; 
                else: ?>
                        <script>
                            let list = document.querySelector('#list');
                            let mens = document.querySelector('#mensage');

                            mens.innerHTML = '<h3 class="flow-text center">Não há clientes cadastrados!</h3>';
                            list.style.display = 'none';
                        </script>
                        <?php
                endif; 
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="box-button">
    <a href="add.php" id="btn_pulse" class="btn-floating btn-large dark_wine z-depth-2 tooltipped"
        data-position="top" data-tooltip="Adicionar cliente"><i class="material-icons">add</i></a>
</div>
<script>
    let btnPulse = document.querySelector('#btn_pulse')
    list.style.display === 'none' ?
        btnPulse.classList.add('pulse') :
        btnPulse.classList.remove('pulse')
</script>
<?php
#footer 
include_once 'includes/footer.php';
?>