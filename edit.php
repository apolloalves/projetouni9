<?php 
session_start(); 
include_once 'conections_db/db_connect.php';
#header
include_once 'includes/header.php'; 

if(isset($_GET['id'])): 
    $id = mysqli_escape_string($connect, $_GET['id']); 
    $sql = "SELECT * FROM CLIENTES WHERE ID = '$id'"; 
    $resultado = mysqli_query($connect, $sql ); 
    $dados = mysqli_fetch_array($resultado); 
endif;
?>

<div class="row">
    <div class="col s12">
        <h3 class='light'><i class="material-icons icon">edit</i>     Editar Cliente</h3>
        <div class="row"></div>
    </div>
    <div class="row">
        <form class="col s12" action="php_action/update.php" method="POST">
            <input type="hidden" name="id" value="<?php echo $dados['ID'];?>">
            <div class="col s12">
                <div class="input-field col s4">
                    <input type="text" name="nome" value="<?php echo $dados['NOME'];?>" id="nome">
                    <label for="nome">Nome</label>
                </div>
                <div class="input-field col s4">
                    <input type="text" name="email" value="<?php echo $dados['EMAIL'];?>" id="email">
                    <label for="email">email</label>
                </div>
                <div class="input-field col s4">
                    <input type="text" name="telefone" value="<?php echo $dados['TELEFONE'];?>" id="telefone">
                    <label for="telefone">Telefone</label>
                </div>
            </div>
            <div class="col s12">
                <div class="input-field col s8">
                    <input type="text" name="endereco" value="<?php echo $dados['ENDERECO'];?>" id="endereco">
                    <label for="endereco">Endereço</label>
                </div>
                <div class="input-field col s4">
                    <input type="text" name="cep" value="<?php echo $dados['CEP'];?>" id="cep">
                    <label for="cep">CEP</label>
                </div>
            </div>
            <div class="col s12">
                <div class="input-field col s8">
                    <input type="text" name="cidade" value="<?php echo $dados['CIDADE'];?>" id="cidade">
                    <label for="cidade">Cidade</label>
                </div>
                <div class="input-field col s4">
                    <input type="text" name="estado" value="<?php echo $dados['ESTADO'];?>" id="estado">
                    <label for="estado">Estado</label>
                </div>
            </div>
            <div class="col s12">
                <div class="col s12">
                    <button type="submit" class="btn teal darken-2" name="btn-editar"><i class="material-icons right">save</i>Salvar</button>
                    <a href="painel.php" class="btn dark_wine"><i class="material-icons right">folder_open</i>Clientes </a>
                  
                </div>
            </div>
        </form>
    </div>
</div>
<?php
    #footer 
    include_once './includes/footer.php';
?>