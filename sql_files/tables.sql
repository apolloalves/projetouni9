CREATE DATABASE CRUD; 
USE CRUD;

CREATE TABLE CLIENTES (
    ID INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    NOME VARCHAR(255) NOT NULL,
	EMAIL VARCHAR(255),
    TELEFONE VARCHAR(20) NOT NULL,
    /*DATA_NASCIMENTO DATE NOT NULL,*/
    ENDERECO VARCHAR(100) NOT NULL,
     CEP VARCHAR(100) NOT NULL,
    CIDADE VARCHAR(100) NOT NULL,
    ESTADO VARCHAR(2) NOT NULL
      
);

CREATE TABLE USUARIO (
    USUARIO_ID INT(11) NOT NULL AUTO_INCREMENT,
    USUARIO VARCHAR(50) NOT NULL,
    SENHA VARCHAR(32) NOT NULL,
    PRIMARY KEY (`usuario_id`)
)  ENGINE=INNODB AUTO_INCREMENT=2 DEFAULT CHARSET=LATIN1; 

INSERT INTO `CLIENTES`(`NOME`,`EMAIL`,`TELEFONE`,`ENDERECO`,`CEP`,`CIDADE`,`ESTADO`) VALUES
('Douglas Apolonio Alves', 'apolloapollo@gmail.com','(11)99205-9242','Rua Da Consolação 393 - APTO 121', '01301-000', 'São Paulo', 'SP'),
('Lorena Santos', 'lorena@yahoo.com','(11)99872-3456','Rua Ana Maria Braga - 88', '02428-034', 'São Paulo', 'SP'),
('Marcelo Olveira ', 'ma1982@oulook.com','(11)98478-2129','Alameda Santos - 1048', '03514-100', 'São Paulo', 'SP'),
('Ana Beatriz Fabiani', 'biagace@gmail.com','(11)97358-7879','Rua Vicente Gally - 233', '01551-7515', 'Diadema', 'SP');

INSERT INTO USUARIO(USUARIO,SENHA) values('apollo', md5('1234'));

/*alter table clientes drop column idade; 
alter table clientes add column idade int(2) not null; 
 */
select * from USUARIO;
select * from CLIENTES;
