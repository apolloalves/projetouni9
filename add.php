<?php 
#header
include_once 'includes/header.php'; 
?>
<div class="row">
    <div class="col s12">
        <h3 class='light'><i class="material-icons icon">person_add</i> Novo Cliente</h3>
        <div class="divider"></div>
        <form class="col s12" action="php_action/create.php" method="POST">
            <div class="col s12">
                <div class="input-field col s6">
                    <input type="text" name="nome" id="nome" minlength="4" maxlength="40" required>
                    <label for="nome">Nome Completo</label>
                </div>
                <div class="input-field col s4">
                    <input type="text" name="email" id="email" required>
                    <label for="email">email</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="telefone" id="telefone" required>
                    <label for="telefone">Telefone</label>
                </div>
            </div>
            <div class="col s12">
                <div class="input-field col s5">
                    <input type="text" name="endereco" id="endereco" required>
                    <label for="endereco">Endereço</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="cep" id="cep" required>
                    <label for="cep">CEP</label>
                </div>
                <div class="input-field col s3">
                    <input type="text" name="cidade" id="cidade" required>
                    <label for="cidade">Cidade</label>
                </div>
                <div class="input-field col s2">
                    <input type="text" name="estado" id="estado" required>
                    <label for="estado">Estado</label>
                </div>
            </div>
            <div class="col s12">
                <div class="col s12">
                    <button type="submit" class="btn teal darken-1" name="btn-cadastrar"><i class="material-icons right">save</i>Inserir</button>
                    <a href="painel.php" class="btn pink darken-4"><i class="material-icons right">folder_open</i>Clientes</a>
                </div>
            </div>
        </form>
    </div>
</div>
<?php
    #footer 
    include_once 'includes/footer.php';
?>