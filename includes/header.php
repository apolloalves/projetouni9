<?php
  session_start(); 
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="UTF-8">
  <meta name="author" content="Apollo Alves"> 
  <meta name="description" content="Projeto Pŕatico de Programação">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="keywords" content="Sistema clientes, Cadastro, Clientes,Adicionar clientes">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  
  <!--Import Google Icon Font-->
  <link href="./assets/styles/icon.css" rel="stylesheet">
  <!--Import materialize.css-->
  <link rel="stylesheet" href="./assets/styles/materialize.min.css">
  <!-- Import styles Ibrella -->
  <link rel="stylesheet" href="./assets/styles/main.css">

  <title>Cadastro Cliente</title>
  <script>
    document.addEventListener('DOMContentLoaded', () => {

      let elems = document.querySelectorAll('.tooltipped');
      let instances = M.Tooltip.init(elems, { enterDelay: 500 })

    })

  </script>
</head>
<body class="site silver">
  <main>
    <div>
      <nav class="dark_wine">
        <div class="nav-wrapper z-depth-3">
          <a href="painel.php" id="cad_logo" class="brand-logo"><i class="material-icons">camera</i>Ibrella</a>
          <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li><a href="add.php" data-position="bottom" class="tooltipped" data-tooltip="adicionar cliente">
                <i class="material-icons medium_icon">person_add</i></a></li>
            <li><a href="painel.php" data-position="bottom" class="tooltipped" data-tooltip="Clientes"><i
                  class="material-icons medium_icon">folder_open</i></a></li>
            <li><a href="logout.php" class="tooltipped" data-tooltip="logout Ibrella"><i
                  class="material-icons medium_icon">exit_to_app</i></a></li>
            <li><a href="#"><?php echo $_SESSION['usuario'];?></a></li>
          </ul>
        </div>
      </nav>
    </div>