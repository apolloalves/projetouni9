 
<?php
session_start(); 
#header
include_once 'includes/header.php'; 

?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
<script>
    M.AutoInit();
    let nav = document.querySelector('nav');
    nav.style.display = 'none';
</script>

<div class="box">
    <div class="boxContent">
    <?php 
            if(isset($_SESSION['nao_autenticado'])):
         ?>
        <div class="row">
            <div class="col s12">
                <p id="erro"><strong>Usuário ou senha inválidos</strong></p>
            </div>
        </div>
        <?php
            
            endif;
            unset($_SESSION['nao_autenticado']);
        ?>
        <div class="col s12">
            <h4 id="title_login" class="mtp">Login</h4>
        </div>
       
        <form action="login.php" method="POST">
            <div class="input-field col s6">
                <input type="text" name="usuario" id="usuario" required>
                <label for="usuario">Usuário</label>
            </div>
            <div class="input-field col s6">
                <input type="password" name="senha" id="pass" required>
                <label for="senha">Senha</label>
            </div>
            <div class="input-field col s12">
                <button class="btn dark_wine" type="submit">Entrar</button>
            </div>
        </form>
    </div>
</div>