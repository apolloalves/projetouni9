                                          PROJETO PRÁTICO DE PROGRAMAÇÃO
                                          
SUMÁRIO

1. OBJETIVOS 4
1.1. DESCRIÇÃO DO SISTEMA 4
2. DIAGRAMS UML 5
3. TELAS DO SISTEMA 6
4. CÓDIGOS DO SISTEMA 7
5. BANCO DE DADOS 8
6. CONCLUSÃO 9
7. REFERÊNCIAS 10

1. OBJETIVOS

Segundo Lakatos e Marconi (2008 p.220) &quot;a especificação do objetivo de uma
pesquisa responde às questões para quê? e para quem?&quot;. Os objetivos determinam
os resultados que a equipe pretende alcançar no desenvolvimento do projeto deste
módulo. Descrever neste item os objetivos ou hipóteses do projeto (meta -
finalidade) deve haver coerência com o que será desenvolvido no projeto e com a
metodologia empregada. Este item pode ser decomposto (dividido) em objetivo geral
e objetivos específicos, podendo utilizar tópicos ou texto corrido. 
(Alterar o texto – incluir o texto da equipe)

1.1. DESCRIÇÃO DO SISTEMA
A equipe deve elaborar um texto com a descrição e plataforma do sistema
onde será desenvolvido.
O projeto será desenvolvido com base um sistema elaborado ou na linguagem
PHP ou na linguagem C#.

2. DIAGRAMA UML

Nesta etapa a equipe deverá implementar um diagrama de classe do sistema
implementado.

3. TELAS DO SISTEMA

Nesta etapa a equipe irá colocar todas as telas do sistema.

4. CÓDIGOS DO SISTEMA

A equipe deverá implementar todos os códigos do sistema.
Deve constar o nome do arquivo com sua extensão, bem como o código PHP,
HTML, CSS, JAVASCRPT ou C# de todos os arquivos do sistema implementado.

5. BANCO DE DADOS

Nesta etapa deverá constar a estrutura de banco de dados utilizada, para isso
a equipe deverá colocar o código SQL utilizado para criação das tabelas do sistema
a ser implementado.

6. CONCLUSÃO

Segundo Severino (2002 p. 83), &quot;a conclusão é a síntese para a qual caminha
o trabalho. Será breve e visará recapitular sinteticamente os resultados&quot; e completa
dizendo que &quot;o autor manifestará seu ponto de vista sobre os resultados obtidos,
sobre o alcance dos mesmos&quot;.


7. REFERÊNCIAS

Inserir no documento as referências bibliográficas utilizadas (padrão ABNT).
Link com para acesso ao Manual para Elaboração de Trabalhos Acadêmicos
de acordo com ABNT:
http://docs.uninove.br/arte/pdfs/Manual-Elaboracao-de-Trabahos-ABNT.pdf
(Alterar o texto – incluir a bibliografia utilizada pela equipeo texto da equipe)

As referências dos documentos consultados para a elaboração do projeto é
um item obrigatório. Nela normalmente constam os documentos e qualquer fonte de
informação consultada no levantamento de literatura. Exemplos: livros, artigos de
revistas, dicionários, normas, sites, entre outros.Seguir padrão ABNT.
Exemplos:
ASSOCIAÇÃO BRASILEIRA DE NORMAS TÉCNICAS. NBR 6023: Informação e
documentação: referências: elaboração. Rio de Janeiro, 2002. 
___________. NBR 6028: Informação e documentação: resumo: apresentação.
Rio de Janeiro, 2003. 
LAKATOS, Eva Maria; MARCONI, Marina de Andrade. Metodologia do trabalho
científico: procedimentos básicos; pesquisa bibliográfica, projeto e relatórios;
publicações e trabalhos científicos. 5. ed. São Paulo: Atlas, 2001.
LAKATOS, Eva Maria; MARCONI, Marina de Andrade. Fundamentos de
metodologia científica. 6. ed. São Paulo: Atlas, 2008.
SEVERINO, Antonio Joaquim. Metodologia do trabalho científico. 22. ed. rev.
ampl. São Paulo: Cortez, 2002.
SISTEMA DE BIBLIOTECAS PROF. JOSÉ STORÓPOLI. Universidade Nove de
Julho. Manual para Elaboração de Trabalhos Acadêmicos de acordo com
ABNT. Disponível em: &lt;http://docs.uninove.br/arte/pdfs/Manual-Elaboracao-de-
Trabahos-ABNT.pdf&gt;. Acesso em 30 de setembro de 2016.
